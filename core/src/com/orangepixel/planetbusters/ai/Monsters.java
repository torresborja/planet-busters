package com.orangepixel.planetbusters.ai;

import com.orangepixel.planetbusters.Bullets;
import com.orangepixel.planetbusters.Entity;
import com.orangepixel.planetbusters.FX;
import com.orangepixel.planetbusters.Globals;
import com.orangepixel.planetbusters.Player;
import com.orangepixel.planetbusters.Render;
import com.orangepixel.planetbusters.World;
import com.orangepixel.utils.Light;

public class Monsters extends Entity {

	
	public final static int	mSHIP = 0,
							mSCENERY = 1,
							mPICKUP = 2,
							mWALKERS = 3,

							mBOUNCEXPLODE = 8;

	
	
	public static Monsters[] monsterList=new Monsters[320];

	
	// position
	int		floatX;
	int		floatY;
	int		xSpeed;
	int		ySpeed;
	int		ySpeedIncrease;
	int		myDirection;
	int		startX;
	int		startY;
	int		targetX;
	int		targetY;
	boolean	onGround;
	
	
	// appearance
	public boolean visible;
	int		xOffsetAdd;
	int		animationDelay;
	int		actionDelay;
	
	// states
	public int		renderPass;
	boolean	activated;
	int		energy;
	int		subType;
	int		myType;
	int		aiState;
	int		aiCountdown;
	int		fireDelay;
	
	
	
	
	public final static void initMonsters() {
		for (int i=monsterList.length - 1; i >= 0; i--) monsterList[i]=new Monsters();
		killAll();
	}
	
	
	public final static void killAll() {
		for (int i=monsterList.length - 1; i >= 0; i--) monsterList[i].deleted=true;
	}
	
	
	
	public final static int addMonster(int mType, int myX, int myY, int mSubType, World myWorld) {
		int i=0;
		while (i<monsterList.length && !monsterList[i].deleted) i++;
		if (i<monsterList.length) {
			monsterList[i].init(mType, myX, myY, mSubType, myWorld);
			return i;
		}
		return -1;
	}
	
	
	
	
	public final static void updateMonsters(World myWorld, Player myPlayer) {
		int i=0;
		
		while (i<monsterList.length) {
			if (!monsterList[i].deleted && !monsterList[i].died) {
				monsterList[i].update(myWorld,myPlayer);
				
				if (monsterList[i].died) {
					// monster died
					monsterList[i].deleted=true;
				}
				
			}
			i++;
		}
	}	
	
	
	
	public final void init(int mType, int myX, int myY, int mSubType, World myWorld) {
		deleted=false;
		died=false;
		
		subType=mSubType;
		myType=mType;
		aiState=0;
		
		visible=true;
		renderPass=1;
		
		activated=false;
		
		x=myX;
		y=myY;
		
		fireDelay=0;
		
		switch (myType) {
			case mSHIP:
				xOffset=0;
				yOffset=33;
				w=29;
				h=15;
				xSpeed=0;
				ySpeed=0;

				targetX=myX<<4;
				targetY=myY<<4;

				x=myWorld.worldOffsetX-128;
				y=myWorld.worldOffsetY+Render.height-64;
				
				startX=x;
				startY=y;
			break;
			
			case mSCENERY:
				m_Scenery.init(this,myWorld);
			break;
			
			case mPICKUP:
				m_Pickup.init(this);
			break;
			
			case mWALKERS:
				m_Walkers.init(this, myWorld);
			break;
			
			
				
			
			case mBOUNCEXPLODE:
				w=10;
				h=10;
				y-=7;
				xOffset=62;
				yOffset=21;
				ySpeed=-8;
				
				energy=2+(myWorld.difficulty);
				aiState=0;
			break;
		}
		
		floatX=x<<4;
		floatY=y<<4;
	}
	
	
	
	
	public final void update(World myWorld, Player myPlayer) {
		boolean onScreen;
		
		if (x+w>myWorld.worldOffsetX-16 && x<myWorld.worldOffsetX+Render.width+16 
			&& y+h>myWorld.worldOffsetY-16 && y<myWorld.worldOffsetY+Render.height+16) onScreen=true;
		else onScreen=false;
		
		if (!onScreen && myType!=mSHIP) return;
		
		
		boolean hitPlayer=false;
		if (myPlayer.x+8>=x && myPlayer.x+1<x+w && myPlayer.y+8>=y && myPlayer.y+1<y+h) hitPlayer=true;
		
		switch (myType) {
			case mSHIP:
				switch (aiState) {
					case 0:
						x=myWorld.worldOffsetX-128;
						y=myWorld.worldOffsetY;
						myWorld.setCamera(x, y);
						myWorld.setCameraTakeOver(x, y, 16);
						
						startX=x;
						startY=y;
						floatX=x<<4;
						floatY=y<<4;
						aiState=1;
					break;
					
					
					case 1:
						// fly in
						xSpeed=(targetX-floatX)>>2;
						ySpeed=(targetY-floatY)>>2;
		
						if (xSpeed>48) xSpeed=48;
						if (ySpeed>48) ySpeed=48;
		
						floatX+=xSpeed;
						floatY+=ySpeed;
						
						x=floatX>>4;
						y=floatY>>4;
						
						myWorld.setCameraTakeOver(x, y, 16);
						
						if (floatX>=targetX-3 && floatY>=targetY-3) {
							aiState=2;
							myPlayer.DropOutShip(x,y);
						}
						
					break;
					
					case 2:
						// hover - idle
					break;
				}
				
				Light.addLight(x-1-myWorld.worldOffsetX, y+7-myWorld.worldOffsetY, 8, Light.LightType_SphereTense, 255,207,17,255);
				Light.addLight(x-1-myWorld.worldOffsetX, y+7-myWorld.worldOffsetY, 64, Light.LightType_Sphere, 255,174,17,255);
			break;

			case mSCENERY:
				m_Scenery.update(this, myWorld, myPlayer,hitPlayer);
			break;
			
			case mPICKUP:
				m_Pickup.update(this, myWorld, myPlayer, hitPlayer);
			break;

			
			case mWALKERS:
				m_Walkers.update(this, myWorld, myPlayer,hitPlayer);
			break;
			
			
			
			

			case mBOUNCEXPLODE:
				doVertical(myWorld);
				
				if (ySpeed<48) ySpeed+=4;
				
				if (onGround) {
					ySpeed=-32;
					onGround=false;
				}
				
				if (aiState==1) {
					doHorizontal(myWorld);
					if (aiCountdown>0) aiCountdown--;
					if (aiCountdown==0 || hitPlayer) {
						aiState=2;
					}
				} else if (aiState==2) {
					died=true;
					FX.addFX(FX.fEXPLOSION, x, y, 0, myWorld);
					Bullets.addBullets(Bullets.bEXPLOSION, x+5, y+5, 32, 0, Bullets.bOWNER_MONSTER, myWorld);
				}
			break;
			
		}
		
	}
	
	
	
	public final void doHorizontal(World myWorld) {
		int tx;
		int ty;
		
		floatX+=xSpeed;
		x=floatX>>4;
		
		ty=(y+(h>>1))>>4;
		
		if (xSpeed>0) 
		{
			tx=(x+w)>>4;
			if (myWorld.isSolid(tx, ty)) 
			{
				x=(tx<<4)-w;
				floatX=x<<4;
				xSpeed=0;
			}
		} 
		else 
		{
			tx=x>>4;
			if (myWorld.isSolid(tx, ty)) 
			{
				x=(tx<<4)+16;
				floatX=x<<4;
				xSpeed=0;
			}
		}
		
	
	}
	
	
	public final void doVertical(World myWorld) {
		int tx;
		int ty;
		
		floatY+=ySpeed;
		y=floatY>>4;
		
		tx=(x+(w>>1))>>4;
		
		if (ySpeed>0) {
			ty=(y+h+3)>>4;
			if (myWorld.isSolid(tx, ty)) {
				onGround=true;
				y=(ty<<4)-(h+3);
				floatY=y<<4;
				ySpeed=0;
			}
		} else {
			onGround=false;
			ty=y>>4;
			if (myWorld.isSolid(tx, ty)) {
				y=(ty<<4)+16;
				floatY=y<<4;
				ySpeed=0;
			}
		}
		
	
	}	
	
	
	
	public final boolean playerInOurRoom(Player myPlayer) {
		if (myPlayer.y+10>y-20 && myPlayer.y<y+20) return true;
		else return false;
	}
	
	public final boolean playerInOurSight(Player myPlayer) {
		if (myPlayer.y+10>y-20 && myPlayer.y<y+20) 
		{
			if (myPlayer.x+5>x && myDirection==Globals.RIGHT) return true;
			if (myPlayer.x+5<x && myDirection==Globals.LEFT) return true;
		}
		
		return false;
	}
	

	public final boolean hit(Bullets myBullet, World myWorld, Player myPlayer) {
		boolean result=false;
		
		
		switch (myType) {
			case mSCENERY:
				return m_Scenery.hit(this, myBullet, myWorld);
			
			case mWALKERS:
				return m_Walkers.hit(this, myBullet, myWorld);
			
			
			
			
			case mBOUNCEXPLODE:
				
				energy-=myBullet.energy;
				if (energy<=0) {
					if (aiState==1) aiState=2;
					else {
						if (myBullet.xSpeed>0) xSpeed=-24;
						else xSpeed=24;
						
						energy=24+(myWorld.difficulty*2);
						
						aiState=1;
						aiCountdown=64;
					}
				}
				result=true;
			break;
		}
		
		
		return result;
	}
	
	
	
}
