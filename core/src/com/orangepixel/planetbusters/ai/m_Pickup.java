package com.orangepixel.planetbusters.ai;

import com.orangepixel.planetbusters.Bullets;
import com.orangepixel.planetbusters.FX;
import com.orangepixel.planetbusters.Player;
import com.orangepixel.planetbusters.World;
import com.orangepixel.utils.Light;

/**
 * Pickups, powerups, etc
 * 
 * @author orangepascal / orangepixel.net
 *
 */
public class m_Pickup {

	public final static int		CREDITS = 0,
								HEARTH = 1;
	
	
	public final static int propXOffset = 0,
							propYOffset = 1,
							propWidth = 2,
							propHeight = 3;
	
	public final static int[][]	properties = new int[][] {
		{17,68,7,7},	// credits
		{40,68,5,7},	// hearth
	};
	

	
	private final static int  	aiIdle = 0,
								aiDrop = 1,
								aiFloat = 2;
	
	/* 
	 * ================
	 * initialise provided monster into our type
	 * ================
	 */
	public final static void init(Monsters e) 
	{
		e.xOffset=properties[e.subType][propXOffset];
		e.yOffset=properties[e.subType][propYOffset];
		e.w=properties[e.subType][propWidth];
		e.h=properties[e.subType][propHeight];
		e.ySpeed=-24;
		e.aiState=aiIdle;	
		
		switch (e.subType)
		{
			case CREDITS:
				e.aiState=aiDrop;
			break;
			
			case HEARTH:
				e.y+=7;
				e.ySpeed=-8;
				e.ySpeedIncrease=-4;
				e.aiState=aiFloat;
			break;
		}
	}

	/*
	 * ================
	 * update specified monster every tick
	 * ================
	 * 	
	 */
	public final static void update(Monsters e, World myWorld, Player myPlayer, boolean hitPlayer) 
	{
		int ty;
		
		switch (e.aiState) 
		{
			
			case aiIdle:
				if (e.aiCountdown>0) 
				{
					e.aiCountdown--;
				}
				else 
				{
					e.died=true;
				}
				
				e.visible=(e.aiCountdown>16 || e.aiCountdown%4<2);
				
				if (hitPlayer) 
				{
					myPlayer.addCredits(5);
					FX.addFX(FX.fSCOREPLUME, e.x,e.y, 5, myWorld);
					e.died=true;
				}
			break;
			
			
			case aiDrop:
				e.floatY+=e.ySpeed;
				e.y=e.floatY>>4;
							
				if (e.ySpeed<64) e.ySpeed+=8;
				ty=(e.y+e.h+4)>>4;
				
				if (myWorld.isSolid(e.x>>4, ty)) 
				{
					e.y=(ty<<4)-(e.h+4);
					e.floatY=e.y<<4;
					e.ySpeed=0;
					e.aiState=aiIdle;
					e.aiCountdown=200;
				}
			break;
			
			
			case aiFloat:
				e.floatY+=e.ySpeed;
				e.y=e.floatY>>4;
				e.ySpeed+=e.ySpeedIncrease;
				
				if (e.ySpeed<=-24 && e.ySpeedIncrease<0) 
				{
					e.ySpeedIncrease=-e.ySpeedIncrease;
				} else if (e.ySpeed>=24 && e.ySpeedIncrease>0) 
				{
					e.ySpeedIncrease=-e.ySpeedIncrease;
				}
				
				if (hitPlayer) 
				{
					myPlayer.addHealth(1);
					e.died=true;
				}
				
				Light.addLight(e.x+3-myWorld.worldOffsetX, e.y+4-myWorld.worldOffsetY, 16, Light.LightType_SphereTense, 255,0,16,255);
				Light.addLight(e.x+3-myWorld.worldOffsetX, e.y+4-myWorld.worldOffsetY, 48, Light.LightType_Sphere, 255,0,16,128);
			break;
		}
		
		if (e.subType==CREDITS)
		{
			Light.addLight(e.x+3-myWorld.worldOffsetX, e.y+3-myWorld.worldOffsetY, 64, Light.LightType_Sphere, 255,206,12,128);
		}
		
	}
	
	/*
	 * ================
	 * handle being hit by the specified bullet
	 * ================
	 */
	public final static boolean hit(Monsters e,Bullets myBullet, World myWorld) 
	{
		
		return true;
	}

	/*
	 * ================
	 *  called after map generation is done, so some objects can turn 
	 *  them selves into "solid" wall type objects before the player starts
	 * ================
	 */
	public final static void solidify(Monsters e,World myWorld) 
	{
	}
	
	
}
