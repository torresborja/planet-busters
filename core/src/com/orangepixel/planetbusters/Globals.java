package com.orangepixel.planetbusters;

import java.util.Random;
import com.badlogic.gdx.Gdx;

/**
 * Global values, debugging function, and random seeding functions
 * 
 * @author orangepascal / orangepixel.net
 *
 */
public class Globals {
	
	public final static boolean debugMe=true;

	public final static int UP = 0,
							RIGHT = 1,
							LEFT = 2,
							DOWN = 3;
	
	// WEAPONS
	public final static int WEAPON_GUN = 0;
	
	// spriteXOffset, spriteYOffset, spriteWidth, spriteHeight
	public final static int[][] weaponValues = new int[][] {
		{0,20, 14,6},  
	};
	
	
	// CHATTER QUOTES
	public final static String[] dudeQuotes = new String[] {
			"let's bring the pain!",
			"let's bust a planet...",
			"here comes the storm",
			"wooooohi!",
			"toooo easy!",
			"this and all that!"
	};
	
	
	
	// GALAXY names
	public final static String[] galaxyNamePre = new String[] {
			"omega",
			"armada",
			"lucious",
			"link",
			"toto",
			"altera",
			"altoro",
			"applepie",
			"methodi",
			"sitrus",
			"sinus",
			"armada",
			"lasius",
			"torius",
			"milky",
			"chocola",
			"sour",
			"bread",
			"left"
	};
	
	public final static String[] galaxyNamePost = new String[] {
			"galaxy",
			"space",
			"region",
			"nebula",
			"sphere",
			"stars",
			"system",
			"u2",
			"populus",
			"cube",
			"planetoids",
			"expanse",
			"area"
	};
	
	

	/* ===============
	 * quick method to easily switch debugging on/off
	 * ===============
	 */
	public final static void debug(String whatsUp) {
		if (!debugMe) return;
		Gdx.app.log("orangepixelsays",whatsUp);
	}
	
	
	
	
	
	// Random and seeding related code
	// seed values
	
	public static Random randomGenerator = new Random();
	static int randx=2016; 
	static int randy=01;
	static int randz=18;
	
	static int randw=1;

	static int[] randomTable=new int[60000];
	static int randomNextInt;
	
	// set this true to always have same level (daily challenge for example)
	static boolean getRandomSeeded=false;
	
	/* ===============
	 * fill randomtable based on specified seeds
	 * ===============
	 */
	public static void fillRandomTable(int seed1, int seed2, int seed3) {
		int t;

		randx=seed1;
		randy=seed2;
		randz=seed3;

		for (int i=0; i < 60000; i++) {
			t=randx ^ (randx << 11);
			randx=randy;
			randy=randz;
			randz=randw;
			randw=randw ^ (randw >> 19) ^ (t ^ (t >> 8));
			randomTable[i]=randw;
		}

		randomNextInt=0;
	}
	
	/* ===============
	 * get random value from seeded table
	 * ===============
	 */
	public static int getRandomSeeded(int digit1) {
		int value=randomTable[randomNextInt] % digit1;
		randomNextInt++;
		if (randomNextInt == 60000) randomNextInt=0;
		return value;
	}
	
	/* ===============
	 * returns either a seeded value or a generator value 
	 * depending on "getRandomSeeded" being true or false
	 * this is a quick way to call one function and have it easily
	 * switch between seeded or non-seeded without changing code
	 */
	public static int getRandom(int digit1) {
		if (getRandomSeeded) return getRandomSeeded(digit1);
		else  return  randomGenerator.nextInt(digit1);
	}		
	
	/* ===============
	 * get forced "random" value from randomGenerator
	 * ===============
	 */
	public static int getRandomNonSeeded(int digit1) {
		return  randomGenerator.nextInt(digit1);
	}

	
}
